#/bin/bash
set -e

wg_iface="${1:-wg0}"
target_dir="/usr/share/doc/wireguard-tools/examples/reresolve-dns"
filename="reresolve-dns.sh"
url="https://raw.githubusercontent.com/WireGuard/wireguard-tools/master/contrib/reresolve-dns/reresolve-dns.sh"

hash1=$(sha256sum "$filename" | cut -f1 -d ' ')

temp_file=$(mktemp) && wget "$url" -qO "$temp_file"
hash2=$(sha256sum "$temp_file" | cut -f1 -d ' ')

echo " Local hash: $hash1"
echo "Actual hash: $hash2"

if [ "$hash1" = "$hash2" ]; then
    echo "Version up to date, continue ..."
else
    echo "Old of modified version, replacing ..."
    cp "$temp_file" "$filename"
fi

rm "$temp_file"

#####################################################################################

echo "Copy wg reresolve script ..."
mkdir -p "$target_dir"
cp "$filename" "$target_dir"/reresolve-dns.sh
chmod +x "$target_dir"/reresolve-dns.sh

echo "Copy wg reresolve systemd files ..."
cp wg-reresolve-dns@.service /lib/systemd/system/wg-reresolve-dns@.service
cp wg-reresolve-dns@.timer /lib/systemd/system/wg-reresolve-dns@.timer

echo "Start wg reresolve service ..."
systemctl enable --now wg-reresolve-dns@"$wg_iface".timer