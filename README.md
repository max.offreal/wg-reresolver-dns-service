# Wireguard reresolver service

## About:
This solution is used to automatically (by time) DNS resolve the domain name of the wireguard endpoint, for some reason when the real ip address changes.

## Dependencies:
- wireguard
- systemd

## Big thanx:
This script uses reresolve-dns.sh script from official github repo: (https://github.com/WireGuard/wireguard-tools/blob/master/contrib/reresolve-dns/reresolve-dns.sh)  
Unit files for systemd was found here: (https://techoverflow.net/2021/08/19/how-to-automatically-re-resolve-dns-in-wireguard-on-linux/)  

## Using:
Just run:
```bash
sudo bash install.sh
```

If your wireguard interface has different name (for example: `wg1`), then run this way:
```bash
sudo bash install.sh wg1
```
